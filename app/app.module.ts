import { HTTP_INTERCEPTORS, HttpClient } from "@angular/common/http";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app.routing";

import { registerElement } from "nativescript-angular/element-registry";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { InputMaskModule } from "nativescript-input-mask/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular/side-drawer-directives";
// import { HomeComponent } from "~/dashboard/home/home.component";
import { MinLengthDirective } from "~/shared/directives/input.directive";
import { AuthGuard } from "./services/auth-guard.service";
import { AuthService } from "./services/auth.service";
import { RefreshTokenService } from "./services/refreshtoken.service";
import { StorageService } from "./services/storage.service";
import { UserService } from "./services/user.service";
import { TokenInterceptor } from "./shared/http/http.interceptor";
import { setStatusBarColors } from "./shared/status-bar-util";
// import { HomeComponent } from "~/dashboard/home/home.component";

// registerElement("CardView", () => require("nativescript-cardview").CardView);
setStatusBarColors();
@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        NativeScriptCommonModule,
        AppRoutingModule,
        NativeScriptHttpClientModule,
        NativeScriptUISideDrawerModule,
        InputMaskModule
    ],
    declarations: [
        AppComponent,
        MinLengthDirective
        // HomeComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        HttpClient,
        AuthGuard,
        AuthService,
        UserService,
        StorageService,
        RefreshTokenService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    exports: [
        MinLengthDirective
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
