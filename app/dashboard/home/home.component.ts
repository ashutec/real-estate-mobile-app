import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { DrawerPage } from "../../drawer/drawer.page";
import { AuthService } from "../../services/auth.service";

@Component({
    moduleId: module.id,
    selector: "ns-home",
    templateUrl: "home.component.html",
    styleUrls: ["home.component.css"]
})
export class HomeComponent extends DrawerPage {
    constructor(
        private page: Page,
        private authService: AuthService,
        private router: RouterExtensions
    ) {
        super();
        this.page.actionBarHidden = true;
    }

    navigateToProfile() {
        this.router.navigate(["profile"]);
    }
    back() {
        this.authService.logout();
        this.router.navigate(["auth/login"], { clearHistory: true, replaceUrl: true });
    }
}
