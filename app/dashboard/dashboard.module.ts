import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { CommonModule } from "@angular/common";
import { registerElement } from "nativescript-angular/element-registry";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular/side-drawer-directives";
import { DrawerComponent } from "~/drawer/drawer-component";
import { DashboardRoutingModule } from "../dashboard/dashboard.routing";
import { HomeComponent } from "../dashboard/home/home.component";
import { AuthGuard } from "../services/auth-guard.service";
import { AuthService } from "../services/auth.service";
registerElement("CardView", () => require("nativescript-cardview").CardView);
// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

@NgModule({
    imports: [
        DashboardRoutingModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        CommonModule,
        NativeScriptUISideDrawerModule
    ],
    declarations: [
        HomeComponent,
        DrawerComponent
    ],
    providers: [
        AuthGuard,
        AuthService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class DashboardModule { }
