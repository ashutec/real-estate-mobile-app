import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AuthGuard } from "~/services/auth-guard.service";
import { UserProfileComponent } from "./user-profile/user-profile.component";

const routes: Routes = [
    { path: "", redirectTo: "profile", pathMatch: "full" },
    { path: "profile", component: UserProfileComponent, canActivate: [AuthGuard]  }

];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ProfileRoutingModule { }
