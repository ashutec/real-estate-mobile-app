import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { CommonModule } from "@angular/common";
import { ProfileRoutingModule } from "./profile.routing";
import { UserProfileComponent } from "./user-profile/user-profile.component";

// registerElement("DropDown", () => require("nativescript-drop-down/drop-down").DropDown);
// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

@NgModule({
    imports: [
        ProfileRoutingModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        CommonModule
    ],
    declarations: [
        UserProfileComponent
    ],
    providers: [
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class ProfileModule { }
