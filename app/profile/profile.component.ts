import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: "ns-profile",
    templateUrl: "profile.component.html",
    styleUrls: ["profile.component.css"]
})
export class UserProfileComponent {
}
