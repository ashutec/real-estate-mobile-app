import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpRequest } from "@angular/common/http";
// import { HTTP_PROVIDERS, Http, Request, RequestMethod, ResponseType } from "@angular/http";
import { Injectable } from "@angular/core";
import * as fs from "file-system";
import { fromFile } from "image-source";
import { RouterExtensions } from "nativescript-angular/router";
import { session, Session } from "nativescript-background-http";
import { Feedback } from "nativescript-feedback";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
// import { empty } from "rxjs/observable/empty";
import { BASE_API_URL } from "../shared/utils";
import { StorageService } from "./storage.service";

@Injectable()
export class UserService {
    file;
    log;
    baseURL;
    private session: Session;
    private feedback: Feedback;
    constructor(
        private http: HttpClient,
        private router: RouterExtensions,
        private storageService: StorageService
    ) {
        this.session = session("image-upload");
        this.feedback = new Feedback();

    }

    userDetails() {
        const getAvatarApi = `${BASE_API_URL}/auth/me`;

        return this.http.post(getAvatarApi, {}, { headers });

    }

    updateUsername(input) {
        const updateUserApi = `${BASE_API_URL}/auth/username`;

        const payload = new FormData();
        payload.append("_method", "PUT");
        payload.append("username", input.username);

        return this.http.post(updateUserApi, payload, {});
    }

    preDefineAvatars() {
        const preDefineAvatarApi = `${BASE_API_URL}/auth/avatars`;

        return this.http.get(preDefineAvatarApi, {});
    }

    getAvatar(): Observable<any> {
        const getAvatarApi = `${BASE_API_URL}/auth/avatar`;
        let headers = new HttpHeaders();

        headers = headers.set("responseType", "blob");

        return this.http.get(getAvatarApi, { headers });

        return this.http.get(getAvatarApi, {
            headers: { "Content-Type": "image/png" },
            responseType: "text"
        });
        // auth.request("image.json").subscribe((res) => {
        //     console.log("IMAGE:::", res.json());
        // });
        // .catch((error: HttpErrorResponse) => this.handleAngularJsonBug(error));

        // this.file = fromFile(res).toBase64String("png");
        // console.log("+++++++++++++++++++++", JSON.parse(this.file));
    }

    uploadAvatar(input) {
        const name = input.substr(input.lastIndexOf("/") + 1);
        const request = {
            url: `${BASE_API_URL}/auth/avatar`,
            method: "POST",
            headers: {
                "Content-Type": "application/octet-stream",
                "File-Name": name
            },
            description: "{ 'uploading': " + name + " }",
            androidAutoDeleteAfterUpload: false,
            androidNotificationTitle: "Uploading Image"
        };
        const test = [{ name: "image", filename: input, mimeType: "image/png" }];

        return this.session.multipartUpload(test, request);
    }

    // private handleAngularJsonBug(error: HttpErrorResponse) {
    //     const jsonParseError = "Http failure during parsing for";
    //     const matches = error.message.match(new RegExp(jsonParseError, "ig"));

    //     if (error.status === 200 && matches.length === 1) {
    //         // return obs that completes;
    //         return empty();
    //     } else {
    //         this.log.debug("re-throwing");

    //         return Observable.throw(error);		// re-throw
    //     }
    // }

    // updateAvatar(input) {
    //     const token = this.storageService.accessToken;
    //     const updateAvatarApi = `${BASE_API_URL}/auth/avatar`;
    //     let headers = new HttpHeaders();

    //     headers = headers.set("Authorization", "bearer " + token);

    //     const httpParams = new HttpParams()
    //         .append("_method", "PUT")
    //         .append("avatar", input.code);

    //     return this.http.post(updateAvatarApi, httpParams, { headers });
    // }
}
