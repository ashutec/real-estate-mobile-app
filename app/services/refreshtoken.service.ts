import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { StorageService } from "./storage.service";

@Injectable()

export class RefreshTokenService {
    private messageSource = new BehaviorSubject(false);
    // tslint:disable-next-line:member-ordering
    currentMessage = this.messageSource.asObservable();

    startTime(trigger: boolean) {
        this.messageSource.next(trigger);
      }
}
