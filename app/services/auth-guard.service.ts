import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { BackEndService } from "./backend.service";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: RouterExtensions) { }

  canActivate() {
    if (BackEndService.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate(["auth/login"], { clearHistory: true });

      return false;
    }
  }
}
