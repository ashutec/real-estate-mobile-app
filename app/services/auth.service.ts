import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Feedback } from "nativescript-feedback";
import { Observable } from "rxjs";
import { Color } from "tns-core-modules/color";
import { BASE_API_URL } from "../shared/utils";
import { RefreshTokenService } from "./refreshtoken.service";
import { StorageService } from "./storage.service";

@Injectable()
export class AuthService {
    private refreshTokenObserver: Observable<any>;
    private feedback: Feedback;

    constructor(
        private http: HttpClient,
        private router: RouterExtensions,
        private storageService: StorageService,
        private refreshTokenService: RefreshTokenService
    ) {
        this.feedback = new Feedback();
    }

    signIn(input) {

        const loginApi = `${BASE_API_URL}/auth/login`;
        const httpParams = new HttpParams()
            .append("identity", input.email)
            .append("password", input.password);

        return this.http.post(loginApi, httpParams).subscribe((res) => {
            const token = (<any>res).result.token;
            this.storageService.accessToken = token;
            this.feedback.show({
                backgroundColor: new Color("#32CD32"),
                message: "LOGIN SUCCESSFUL",
                duration: 200
            });
            this.refreshTokenService.startTime(true);
            this.router.navigate(["dash/home"], { clearHistory: true, replaceUrl: true });
        });
    }

    registerUser(body) {
        const registerURL = `${BASE_API_URL}/auth/register`;

        return this.http.post(registerURL, body).subscribe((res) => {
            this.feedback.show({
                backgroundColor: new Color("#32CD32"),
                message: "REGISTRATION SUCCESSFUL",
                duration: 200
            });
            this.router.navigate(["auth/login"], { clearHistory: true, replaceUrl: true });
        });
    }

    getRegisterUserDetails() {
        const userDetails = `${BASE_API_URL}/auth/me`;

        return this.http.get(userDetails);
    }

    forgetPassword(input) {
        const forgetPasswordApi = `${BASE_API_URL}/auth/password`;
        const httpParams = new HttpParams()
            .append("code", input.code)
            .append("password", input.password)
            .append("password_confirmation", input.password_confirmation)
            .append("_method", "PUT");

        return this.http.post(forgetPasswordApi, httpParams).subscribe((res) => {
            this.feedback.show({
                backgroundColor: new Color("#32CD32"),
                message: "PASSWORD SET",
                duration: 200
            });
            this.router.navigate(["auth/login"], { clearHistory: true, replaceUrl: true });
        });
    }

    SMS(input) {
        const verifyOtpApi = `${BASE_API_URL}/auth/sms`;
        const httpParams = new HttpParams()
            .append("username", input.username);

        return this.http.post(verifyOtpApi, httpParams);
    }

    refreshTokenShared() {
        const refreshToken = this.storageService.accessToken;
        const refreshTokenApi = `${BASE_API_URL}/auth/refresh`;
        let headers = new HttpHeaders();

        headers = headers.set("Authorization", "bearer " + this.storageService.accessToken);

        return this.http.post(refreshTokenApi, {}, { headers }).subscribe((res) => {
            const token = (<any>res).result.token;
            this.storageService.accessToken = token;

            return token;
        }, (error) => {
            console.log("error", error);
        });
    }

    logout() {
        const token = this.storageService.accessToken;
        const logout = `${BASE_API_URL}/auth/logout`;
        let headers = new HttpHeaders();

        headers = headers.set("Authorization", "bearer " + token);

        return this.http.post(logout, {}, { headers }).subscribe((res) => {
            this.storageService.removeAllToken();
            this.refreshTokenService.startTime(false);

            return token;
        });
    }
}
