import { Injectable } from "@angular/core";
import { getBoolean, getNumber, getString, remove, setBoolean, setNumber, setString } from "application-settings";

@Injectable()

export class StorageService {

    // Login access Token
    set accessToken(value) {
        setString("token", value);
    }

    get accessToken(): string {
        return getString("token");
    }

    // Refresh Token
    set refreshToken(value) {
        setString("refreshToken", value);
    }

    get refreshToken(): string {
        return getString("refreshToken");
    }

    removeAllToken() {
        remove("token");
        remove("refreshToken");
    }
}
