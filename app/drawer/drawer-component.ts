import { Component, EventEmitter, Output } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { AuthService } from "../services/auth.service";

@Component({
    moduleId: module.id,
    selector: "ns-drawer-component",
    templateUrl: "drawer-component.html",
    styleUrls: ["drawer-component.css"]

})
export class DrawerComponent {
    @Output() output = new EventEmitter();
    photoUrl;
    firstName;
    lastName;
    currentState;

    constructor(
        private authService: AuthService,
        private router: RouterExtensions
    ) {
    }
    navigateToProfile() {
        this.router.navigate(["profile"]);
    }
    closeDrawer() {
        this.output.emit("close Drawer");
    }

    logout() {
        this.authService.logout();
        this.router.navigate(["auth/login"], { clearHistory: true, replaceUrl: true });
    }
}
