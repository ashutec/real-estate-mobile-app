import { Component, ViewChild } from "@angular/core";
import { NgModel } from "@angular/forms";
import { RouterExtensions } from "nativescript-angular/router";
import { Feedback } from "nativescript-feedback";
import { Color } from "tns-core-modules/color";
import { Page } from "ui/page";
import { AuthService } from "../../services/auth.service";
import { Login } from "./login";

@Component({
    moduleId: module.id,
    selector: "ns-login",
    templateUrl: "login.component.html",
    styleUrls: ["login.component.css"]
})
export class LoginComponent {
    @ViewChild("emailOrPhone") emailOrPhone: NgModel;
    @ViewChild("password") password: NgModel;

    input: Login;
    isValidUser = true;
    private feedback: Feedback;

    constructor(private page: Page, private authService: AuthService, private router: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.input = new Login();
        this.input.email = "mobileuser";
        this.input.password = "test123456";
    }

    signIn() {
        this.authService.signIn(this.input);
    }

    register() {
        this.router.navigate(["auth/register"]);
    }

    forgotPassword() {
        this.router.navigate(["auth/sms"]);
    }
}
