import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { AuthService } from "../../services/auth.service";

@Component({
    moduleId: module.id,
    selector: "ns-sms",
    templateUrl: "sms.component.html",
    styleUrls: ["sms.component.css"]
})
export class CodeComponent {
    input;
    constructor(
        private page: Page,
        private authService: AuthService,
        private router: RouterExtensions
    ) {
        this.page.actionBarHidden = true;
        this.input = {
            username: ""
        };
    }

    navigateToConfirmScreen() {
        this.authService.SMS(this.input);
        this.router.navigate(["auth/forgot"]);
    }
}
