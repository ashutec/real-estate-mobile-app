import { Component, ViewChild } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: "ns-authentication",
    templateUrl: "authentication.component.html",
    styleUrls: ["authentication.component.css"]
})
export class AuthenticationComponent {
}
