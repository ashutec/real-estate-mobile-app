import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AuthenticationComponent } from "./authentication.component";
import { ForgotComponent } from "./forgot-password/forgot.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { CodeComponent } from "./sms/sms.component";

const routes: Routes = [
    { path: "", redirectTo: "login", pathMatch: "full"},
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "sms", component: CodeComponent },
    { path: "forgot", component: ForgotComponent }

];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AuthenticationRoutingModule { }
