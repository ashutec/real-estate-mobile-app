import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { CommonModule } from "@angular/common";
import { AuthGuard } from "../services/auth-guard.service";
import { AuthService } from "../services/auth.service";
import { AuthenticationComponent } from "./authentication.component";
import { AuthenticationRoutingModule } from "./authentication.routing";
import { ForgotComponent } from "./forgot-password/forgot.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { CodeComponent } from "./sms/sms.component";

// registerElement("DropDown", () => require("nativescript-drop-down/drop-down").DropDown);
// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

@NgModule({
    imports: [
        AuthenticationRoutingModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        CommonModule
    ],
    declarations: [
        AuthenticationComponent,
        LoginComponent,
        RegisterComponent,
        CodeComponent,
        ForgotComponent
    ],
    providers: [
        AuthGuard,
        AuthService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AuthenticationModule { }
