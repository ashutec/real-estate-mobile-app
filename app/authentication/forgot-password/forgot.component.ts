import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { AuthService } from "../../services/auth.service";

@Component({
    moduleId: module.id,
    selector: "ns-forgot",
    templateUrl: "forgot.component.html",
    styleUrls: ["forgot.component.css"]
})
export class ForgotComponent {
    input;
    constructor(
        private page: Page,
        private authService: AuthService,
        private router: RouterExtensions
    ) {
        this.page.actionBarHidden = true;
        this.input = {
            code: "",
            password: "",
            confirm_password: ""
        };
    }
    checkPassword() {
        if (this.input.password !== null && this.input.password_confirmation !== null) {
            if (this.input.password === this.input.password_confirmation) {
                return false;
            } else {
                return true;
            }
        }
    }

    navigateToLogin() {
        this.authService.forgetPassword(this.input);
    }

}
