export class Login {
    firstname: string;
    lastname: string;
    mobile: string;
    email: string;
    password: string;
    password_confirmation: string;
    job_title: string;
    agency: string;
    web: string;
    dob: string;
    addr_line1: string;
    addr_line2: string;
    addr_suburb: string;
    addr_state: string;
    addr_postcode: number;
    emergency_name: string;
    emergency_phone: number;
}