import { Component, ElementRef, NgZone, OnInit, ViewChild } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { Feedback } from "nativescript-feedback";
import { Color } from "tns-core-modules/color";
import { Page } from "ui/page";
import { AuthService } from "../../services/auth.service";
import { Login } from "./register";
registerElement("FilterableListpicker", () => require("nativescript-filterable-listpicker").FilterableListpicker);
@Component({
    moduleId: module.id,
    selector: "ns-register",
    styleUrls: ["register.component.css"],
    templateUrl: "register.component.html"
})
export class RegisterComponent {
    @ViewChild("myFilter") myFilter: ElementRef;
    input: Login;
    private feedback: Feedback;
    constructor(
        private page: Page,
        private authService: AuthService) {
        this.page.actionBarHidden = true;
        this.feedback = new Feedback();
        this.input = new Login();
    }
    onExtractedValueChange(args) {
        this.input.mobile = args.value;
    }

    checkPassword() {
        if (this.input.password !== null && this.input.password_confirmation !== null) {
            if (this.input.password === this.input.password_confirmation) {
                return false;
            } else {
                return true;
            }
        }
    }

    register() {
        const signUp = new FormData();
        signUp.append("app", "prospector");
        // tslint:disable-next-line:forin
        for (const key in this.input) {
            signUp.append(key, this.input[key]);
        }
        this.authService.registerUser(signUp);
    }
}
