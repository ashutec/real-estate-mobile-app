import { Component, OnInit } from "@angular/core";
import { AuthService } from "./services/auth.service";
import { RefreshTokenService } from "./services/refreshtoken.service";

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})

export class AppComponent implements OnInit {
    refreshToken;
    constructor(private authService: AuthService,
                private refreshTokenService: RefreshTokenService) {
    }
    ngOnInit() {
        this.refreshTokenService.currentMessage.subscribe((trigger) => {
            if (trigger) {
                this.refreshOneHr();
            } else {
                this.destroyToken();
            }
        });
    }
    refreshOneHr() {
        this.refreshToken = setInterval(() => {
            this.authService.refreshTokenShared();
        }, 3000000);
    }

    destroyToken() {
        clearInterval(this.refreshToken);
    }
}
