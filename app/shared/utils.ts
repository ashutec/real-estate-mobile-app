export const BASE_API_URL = "https://api.agentmaster.com.au";

export function createLoadingOption(message) {
    return {
        message,
        progress: 0.65,
        android: {
            indeterminate: true,
            cancelable: false,
            cancelListener: (dialog) => console.log("Loading cancelled"),
            max: 100,
            progressNumberFormat: "%1d/%2d",
            progressPercentFormat: 0.53,
            progressStyle: 1,
            secondaryProgress: 1
        }
    };
}

export function isJSON(data: string) {
    try {
        JSON.parse(data);
    } catch (e) {
        return false;
    }

    return true;
}