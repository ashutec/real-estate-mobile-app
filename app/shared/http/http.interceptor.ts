// tslint:disable-next-line:max-line-length
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Feedback } from "nativescript-feedback";
import { LoadingIndicator } from "nativescript-loading-indicator";
import { Observable, throwError as observableThrowError } from "rxjs";
import { catchError, mergeMap, tap } from "rxjs/operators";
import { AuthService } from "../../services/auth.service";
import { StorageService } from "../../services/storage.service";
import { BASE_API_URL, createLoadingOption } from "../utils";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    loader = new LoadingIndicator();
    options = createLoadingOption(`Loading...`);
    private feedback: Feedback;
    constructor(private inj: Injector,
                private auth: AuthService) {
        this.feedback = new Feedback();
    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const storageService = this.inj.get(StorageService);
        const accessToken = storageService.accessToken;

        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ` + accessToken
            }
        });

        if (request.url.includes(BASE_API_URL)) {
            this.loader.show();
        }

        return next.handle(request)
            .pipe(
                tap((event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        this.loader.hide();
                    }
                })
                , catchError((error) => {
                    this.loader.hide();
                    if (error instanceof HttpErrorResponse) {
                        if (error.status !== 200) {
                            this.feedback.error({
                                title: error.error.message,
                                duration: 1000,
                                onTap: () => { this.feedback.hide(); }
                            });
                        }
                    }

                    return observableThrowError(error);
                }));
    }
}
