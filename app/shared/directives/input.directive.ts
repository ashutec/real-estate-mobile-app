import { Directive, Input } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
    selector: "[minlength]",
    providers: [{ provide: NG_VALIDATORS, useExisting: MinLengthDirective, multi: true }]
})
export class MinLengthDirective implements Validator {

    @Input() minlength: string;

    constructor() { }

    validate(control: AbstractControl): { [key: string]: any } {
        return !control.value || control.value.length >= this.minlength ? null : { "minlength": true };
    }
}
