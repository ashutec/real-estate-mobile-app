import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "dash", pathMatch: "full" },
    {
        path: "auth",
        loadChildren: "./authentication/authentication.module#AuthenticationModule"
    },
    {
        path: "dash",
        loadChildren: "./dashboard/dashboard.module#DashboardModule"
    },
    {
        path: "profile",
        loadChildren: "./profile/profile.module#ProfileModule"
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
